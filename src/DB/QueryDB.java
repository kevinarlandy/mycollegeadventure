/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import InGame.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author willy
 */
public class QueryDB {
    
    public static void insertData(Player P){
        Koneksi conMan = new Koneksi();
        Connection con = conMan.logOn();
        
        try {
            String query = "insert into player(nama, point)"
                            +"value ('" + P.getNama() + "','" + P.getPoint()+ "')";
            System.out.println(query);
            Statement st = con.createStatement();
            boolean rs = st.execute(query);
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
    public static List<Player> LoadGame(){
        List<Player> listP = new ArrayList<>();
        
        Koneksi conMan = new Koneksi();
        Connection con = conMan.logOn();
        
        String query = "Select * from player";
        try{
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                Player P = new Player();
                P.setNama(rs.getString("nama"));
                P.setPoint(rs.getInt("point"));
                P.setTimeplay(rs.getString("timeplay"));
                listP.add(P);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return listP;
    }
}

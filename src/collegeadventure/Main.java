/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegeadventure;

import static collegeadventure.Game.playSound;
import tubesgame.display.Display;

/**
 *
 * @author Kevin Arlandy
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Game game = new Game("Stage01", 800, 600);
        game.start();
        playSound();
       
    }
    
}

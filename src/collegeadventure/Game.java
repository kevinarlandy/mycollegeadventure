/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegeadventure;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import tubesgame.display.Display;
import tubesgame.entities.movable.Player;
import tubesgame.gfx.Assets;
import tubesgame.gfx.GameCamera;
import tubesgame.gfx.ImageLoader;
import tubesgame.gfx.SpriteSheet;
import tubesgame.input.KeyManager;
import tubesgame.states.GameState;
import tubesgame.states.MenuState;
import tubesgame.states.State;
import tubesgame.turf.Turf;


/**
 *
 * @author Kevin Arlandy
 */
public class Game implements Runnable{
    
    private Display display;
    private int width, height;
    public String title;
    //frame per ticks(fps)
    public int fpt = 0;
    
    private boolean running = false;
    private Thread thread;
    
    //mengurangi flick
    private BufferStrategy bs;
    private Graphics g;
    
    //States
    private State gameState;
    private State menuState;
    
    //Input
    private KeyManager keyManager;
    
    //Camera
    private GameCamera gameCamera;
    
    //Handler
    private Handler handler;
    
    public Game(String title, int width, int height){
        this.width = width;
        this.height = height;
        this.title = title;
        keyManager = new KeyManager();
    }
    
    
    private void init(){
        display = new Display(title, width, height);
        display.getFrame().addKeyListener(keyManager);
        Assets.init();
        
        handler = new Handler(this);
        gameCamera = new GameCamera(handler, 0, 0);
        
        gameState = new GameState(handler);
        menuState = new MenuState(handler);
        State.setState(gameState);    
    }
    
    private void tick(){
        keyManager.tick();
        if(State.getState() != null){
           State.getState().tick();
       }
    }
    
    private void render(){
        bs = display.getCanvas().getBufferStrategy();
        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();
        //clear screen biar ga numpuk renderny
        g.clearRect(0, 0 , width, height);
        
        //start
        
        if(State.getState() != null){
            
            State.getState().render(g);
            //fpt/fps
            
            g.setColor(Color.RED);
            g.setFont(new Font("Dialog",Font.BOLD,18));
            g.drawString(""+fpt, 24,24);
            
        }
        
        //end
        bs.show();
        g.dispose();
    }
    
    
    public void run(){
        
        init();
        
        //frame per detik atau tick per detik
        int fps = 60;
        double timePerTick = 1000000000L / fps;
        double delta = 0;
        long now; //current time
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;
        
        //fpt
        long lt = System.nanoTime();
        int frames = 0;
        
        while(running){
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;
         
            if(delta >= 1){
                tick();
                render();
                ticks++;
                delta--;
            }
            
            if(timer >= 1000000000L){
                System.out.println("FPS : " + ticks);
                ticks = 0;
                timer = 0;
            }
            
            frames++;
            if(System.nanoTime() - lt >= 10000){
                fpt = frames;
                frames = 0;
                lt = System.nanoTime();
            }
        }
        stop();
    }
    
    //
    public KeyManager getKeyManager(){
        return keyManager;
    }
    
    public GameCamera getGameCamera(){
        return gameCamera;
    }
    
    //synchronized kepake setiap thread diproses
    
    
    public static Clip clip;
    
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("img/worlds/backgroundsound.wav").getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    
    public static void npc(){
        
    }
    
    public synchronized void start(){
        if(running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }
   
    
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }
    
    public synchronized void stop(){
        if(!running)
            return;
        running = false;
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
        
    //game <- main menu -> settings
    
    //entity
    //- anything that is not a tile is entity
    
    //entity -> position (x,y) (koordinat
    //       -> tick
    //       -> render
    //   ^
    //creature -> health
    //   ^
    //player -> texture
    //       -> input
}

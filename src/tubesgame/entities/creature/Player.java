/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.creature;

//import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import collegeadventure.Game;
import collegeadventure.Handler;

//import tubesgame.Game;
import collegeadventure.Handler;

import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;

/**
 *
 * @author Kevin Arlandy
 */
public class Player extends Creature {

    
    //animations 
    private Animations mvDown;
    private Animations mvUp;
    private Animations mvLeft;
    private Animations mvRight;
    private Animations mvStay;
    
    //atribut player
    int point;
    String timeplay;

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getTimeplay() {
        return timeplay;
    }

    public void setTimeplay(String timeplay) {
        this.timeplay = timeplay;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

//    public Player(String nama, int point, String timeplay){
//        
//    }
    
      
    
    public Player(Handler handler, float x, float y) {
        super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        
        //ukuran collision
        bounds.x = 16;
        bounds.y = 32;
        bounds.width = 32;
        bounds.height = 32;
        
        
        //animations
        // 500 = setengah detik
        mvDown = new Animations(350, Assets.player_down);
        mvUp = new Animations(350, Assets.player_up);
        mvLeft = new Animations(350, Assets.player_left);
        mvRight = new Animations(350 , Assets.player_right);
        mvStay = new Animations(0, Assets.player_stay);
    }
    
    @Override
    public void tick() {
        //animations 
        mvDown.tick();
        mvUp.tick();
        mvLeft.tick();
        mvRight.tick();
        mvStay.tick();
        
        //movement player
        getInput();
        move();
        handler.getGameCamera().centerOnEntity(this);
    }
    
    private void getInput(){
        xMove = 0;
        yMove = 0;
        
        if(handler.getKeyManager().up)
            yMove = -speed;
        if(handler.getKeyManager().down)
            yMove = speed;
        if(handler.getKeyManager().left)
            xMove = -speed;
        if(handler.getKeyManager().right)
            xMove = speed;
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),  (int)(x - handler.getGameCamera().getxOffset()), 
                                    (int)(y - handler.getGameCamera().getyOffset()), width, height, null);
        
//        
//        transparent collision  + test collision
//        int value = 0;
//        Color transparent = new Color(0, 0,0, value);
//        g.setColor(transparent);
//        g.fillRect((int)(x + bounds.x - handler.getGameCamera().getxOffset()),
//                    (int)(y+ bounds.y - handler.getGameCamera().getyOffset()),
//                    bounds.width, bounds.height);
    }
    
    
    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
    
    
    private BufferedImage getCurrentAnimationFrame(){
        if(xMove == 0 && yMove ==0){
           return mvStay.getCurrentFrame();
        }else{
            if(xMove < 0){
                return mvLeft.getCurrentFrame();
            }else if(xMove > 0){
                return mvRight.getCurrentFrame();
            }else if(yMove < 0){
                return mvUp.getCurrentFrame();
            }else{
                return mvDown.getCurrentFrame();
            }
            
        }
    }

    @Override
    public String toString() {
        return "Nama : " + nama 
                + "\nPoint : " + point
                + "\nTimeplay : " +timeplay;
    } 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.unmovable;

import collegeadventure.Handler;
import java.awt.Graphics;
import tubesgame.gfx.Assets;

/**
 *
 * @author Kevin Arlandy
 */
public class Chair2 extends Unmovable {
    
    public Chair2(Handler handler, float x, float y) {
        super(handler, x, y, 55, 55);
        
       
    }

    @Override
    public void tick() {
   
    }

    public void render(Graphics g) {
        g.drawImage(Assets.chair2   ,  (int)(x - handler.getGameCamera().getxOffset()), 
                                       (int)(y - handler.getGameCamera().getyOffset()), width, height, null);  
    }
}

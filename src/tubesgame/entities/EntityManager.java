/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities;

import collegeadventure.Handler;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Comparator;
import tubesgame.entities.movable.Player;

/**
 *
 * @author Kevin Arlandy
 */
public class EntityManager {

    private Handler handler;
    private Player player;
    
    private ArrayList<Entity> entities;
    
    
    private Comparator<Entity> renderSorter = new Comparator<Entity>(){
        @Override
        public int compare(Entity z, Entity x) {
            if(z.getY() + z.getHeight() < x.getY() + x.getHeight())
                return -1;
            return 1;
        }
        
    };
    
    public EntityManager(Handler handler, Player player){
        this.handler = handler;
        this.player = player;
        entities = new ArrayList<Entity>();
        //player masuk arraylist biar tick+render bareng sm entity yg lain
        addEntity(player);
        
    }
    
    public void tick(){
        for(int i = 0; i< entities.size();i++){
            Entity e = entities.get(i);
            e.tick();
        }   
        entities.sort(renderSorter);
    }
    
    public void render(Graphics g){
        for(Entity e : entities){
            
            e.render(g);    
        }
    }
    
    public void addEntity(Entity e){
        entities.add(e);
    }
    
    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }

    public void setEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }
    
    
}

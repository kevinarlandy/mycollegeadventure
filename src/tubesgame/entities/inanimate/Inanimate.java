/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.inanimate;

import collegeadventure.Handler;
import tubesgame.entities.Entity;

/**
 *
 * @author Kevin Arlandy
 */

//Structure = tembok , dll
public abstract class Inanimate extends Entity {
    
    public Inanimate(Handler handler, float x, float y, int width, int height){
        super(handler, x, y,width,height);    
    }
    
    
}

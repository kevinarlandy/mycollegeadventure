/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.inanimate;

import java.awt.Graphics;
import collegeadventure.Handler;
import tubesgame.gfx.Assets;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class Table extends Inanimate {
    
  
    public Table(Handler handler, float x, float y){
        super(handler, x, y, Turf.TURFWIDTH, Turf.TURFHEIGHT);
    }

    @Override
    public void tick() {
       
    }

    @Override
    public void render(Graphics g) {
      
    }
}

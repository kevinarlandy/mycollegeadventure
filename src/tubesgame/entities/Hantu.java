/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import collegeadventure.Handler;
import tubesgame.entities.creature.Creature;
import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;

/**
 *
 * @author Davin Yulion
 */
public class Hantu extends MovementHantu implements Runnable{

    int xMove = 0;
    int yMove = 0;
    private Animations mvDown;
    private Animations mvUp;
    private Animations mvLeft;
    private Animations mvRight;
    private Animations mvStay;
    protected float speed;

    public Hantu(Handler handler, float x, float y) {
        super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        
        bounds.x = 16;
        bounds.y = 32;
        bounds.width = 32;
        bounds.height = 32;

        //animations
        // 500 = setengah detik
        mvDown = new Animations(350, Assets.player_down);
        mvUp = new Animations(350, Assets.player_up);
        mvLeft = new Animations(350, Assets.player_left);
        mvRight = new Animations(350, Assets.player_right);
        mvStay = new Animations(0, Assets.player_stay);
    }

    @Override
    public void tick() {
        //animations 
        mvDown.tick();
        mvUp.tick();
        mvLeft.tick();
        mvRight.tick();
        mvStay.tick();
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(), (int) (x - handler.getGameCamera().getxOffset()),
                (int) (y - handler.getGameCamera().getyOffset()), width, height, null);
    }

    private BufferedImage getCurrentAnimationFrame() {

        if (xMove == 0 && yMove == 0) {
            return mvStay.getCurrentFrame();
        } else {
            if (xMove < 0) {
                return mvLeft.getCurrentFrame();
            } else if (xMove > 0) {
                return mvRight.getCurrentFrame();
            } else if (yMove < 0) {
                return mvUp.getCurrentFrame();
            } else {
                return mvDown.getCurrentFrame();
            }

        }
    }
     @Override
    public void run() {
        while (true) {
           
           //Mengambil posisi dari hantu dan if(y == awal) then  x++ jika posisi x sudah mencapai yang sudah di tenteukan
           //Jika sudah di tenttukan menggunakan if(x == capai) then y++ if(y == capai) then x-- if(x == awal) then y--
        }
    }
   

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import collegeadventure.Game;
import collegeadventure.Handler;

/**
 *
 * @author Kevin Arlandy
 */
public abstract class Entity {
    
    protected Handler handler;
    //float -> biar gerakannya smooth
    protected float x, y;
    protected int width, height;
    /////
    protected Rectangle bounds;
    
    public Entity(Handler handler, float x, float y, int width, int height){
        this.handler = handler;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        
        //pembatas posisi awal
        bounds = new Rectangle(0, 0, width, height);
    }
    
    public abstract void tick();
    
    public abstract void render(Graphics g);
    
    public boolean checkEntityCollisions(float xOffset, float yOffset){
        for(Entity e : handler.getWorld().getEntityManager().getEntities()){
            if(e.equals(this))
                continue;
<<<<<<< HEAD
            if(e.getCollisionBounds(5, 5).intersects(getCollisionBounds(xOffset,yOffset)))
=======
            if(e.getCollisionBounds(0f, 0f).intersects(getCollisionBounds(xOffset,yOffset)))
>>>>>>> 4763d0f857a05a127c3d7684d831107964e8198f
                return true;
        }
        return false;
    }
    
    public Rectangle getCollisionBounds(float xOffset, float yOffset){
        return new Rectangle((int) (x + bounds.x + xOffset), (int) (y + bounds.y + yOffset), bounds.width, bounds.height);
        
    }
    

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }  
    
}

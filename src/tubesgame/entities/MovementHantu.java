/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities;

import collegeadventure.Handler;
import tubesgame.turf.Turf;

/**
 *
 * @author Davin Yulion
 */
public abstract class MovementHantu extends Entity {

    protected float xMove, yMove;

    public MovementHantu(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        xMove = 0;
        yMove = 0;
    }

    public float getxMove() {
        return xMove;
    }

    public void setxMove(float xMove) {
        this.xMove = xMove;
    }

    public float getyMove() {
        return yMove;
    }

    public void setyMove(float yMove) {
        this.yMove = yMove;
    }

  

   

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Handler;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class Lecture extends Movable {
    
    private Animations lectureLeft, lectureRight;

    private float dest = 1900;
    
    public Lecture(Handler handler, float x, float y){
        super(handler, x, y, Turf.TURFWIDTH, Turf.TURFHEIGHT);
        bounds.x = 16;
        bounds.y = 32;
        bounds.width = 32;
        bounds.height = 32;
       
        lectureLeft = new Animations(500, Assets.lecture_left);
        lectureRight = new Animations(500, Assets.lecture_right);
    }

    @Override
    public void tick() {
        
        lectureLeft.tick();
        lectureRight.tick();
        moveAround();
        lectureMove();
        
    }
    
    public void moveAround(){
      
        
        if(dest == 1900){
            xMove = -speed/3;
            if(getxPosition() <= dest){
                dest = 2200;
            }
        }
        
        if(dest == 2200){
            xMove = speed/3;
            if(getxPosition() >= dest){
                dest = 1900;
            }
        }
     



        
   
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),  (int)(x - handler.getGameCamera().getxOffset()), 
                                    (int)(y - handler.getGameCamera().getyOffset()), width, height, null);
    }
    
    private BufferedImage getCurrentAnimationFrame(){
        if(xMove < 0){
            return lectureLeft.getCurrentFrame();
        }else{
            return lectureRight.getCurrentFrame();
        }
    }
}

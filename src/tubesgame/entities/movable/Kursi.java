/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Handler;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import tubesgame.gfx.Assets;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class Kursi extends Movable{
    


    private float dest = 1470;
    
    public Kursi(Handler handler, float x, float y){
        super(handler, x, y, 44, 44);

    }

    @Override
    public void tick () {
        

        moveAround();
        chairMove();
        
    }
    
    public void moveAround(){
      
        
        if(dest == 1470){
            yMove = +speed/10;
            if(getyPosition() >= dest){
                dest = 1400;
            }
        }
        
        if(dest == 1400){
            yMove = -speed/10;
            if(getyPosition() <= dest){
                dest = 1470;
            }
        }

   
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.chair  ,  (int)(x - handler.getGameCamera().getxOffset()), 
                                       (int)(y - handler.getGameCamera().getyOffset()), width, height, null);  
    }
}

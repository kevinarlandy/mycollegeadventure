/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.creature;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import collegeadventure.Handler;
import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;

/**
 *
 * @author Kevin Arlandy
 */
public class Enemies extends Creature{
    
    private Animations mvGhost1;

    
    public Enemies(Handler handler, float x, float y){
        super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        
        
        mvGhost1 = new Animations(0 , Assets.npc_ghost1);
    }
    
    
    @Override
    public void tick() {
        
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),  (int)(x - handler.getGameCamera().getxOffset()), 
                                    (int)(y - handler.getGameCamera().getyOffset()), width, height, null);
    }
    
    
    private BufferedImage getCurrentAnimationFrame(){
        return mvGhost1.getCurrentFrame();
    }
    
    

  
    
}

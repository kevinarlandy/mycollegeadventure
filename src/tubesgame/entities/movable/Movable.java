/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Game;
import collegeadventure.Handler;
import tubesgame.entities.Entity;
import tubesgame.entities.EntityManager;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public abstract class Movable extends Entity{
    
    EntityManager entityManager;
    
    Player p;
    
    public static final int DEFAULT_HEALTH = 10;
    public static final float DEFAULT_SPEED = 3;
    public static final int DEFAULT_CREATURE_WIDTH = 64;
    public static final int DEFAULT_CREATURE_HEIGHT = 64;
    
    String nama;
    protected int health;
    protected float speed, lady_speed;
    protected float xMove, yMove;
    protected float dest;
    protected float xLast , yLast;
    
    public Movable(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        health = DEFAULT_HEALTH;
        speed = DEFAULT_SPEED;
        lady_speed = 2;
        xMove = 0;
        yMove = 0;
      
        
        xLast = 0;
        yLast = 0;
       
    }
    
    //collision
    public void move(){
        if(!checkEntityCollisions(xMove, 0f))
            moveX();
        if(!checkEntityCollisions(0f, yMove))
            moveY();
    }
    
    public void tuyulMove(){
        if(!checkEntityCollisions(xMove, 0f))
            moveX();
        if(!checkEntityCollisions(0f, xMove))
            moveY();
    }
    
    public void ladyMove(){
        if(!checkEntityCollisions(xMove, 0f))
            moveX();
        if(!checkEntityCollisions(0f, xMove))
            moveY();
    }
    
    public void kadepMove(){
        if(!checkEntityCollisions(xMove, 0f))
            moveX();
        if(!checkEntityCollisions(0f, xMove))
            moveY();
    }
    
     public void lectureMove(){
        if(!checkEntityCollisions(xMove,0f))
            moveX();
        if(!checkEntityCollisions(0f, xMove))
            moveY();
    }
    
    public void chairMove(){
        if(!checkEntityCollisions(xMove,0f))
            moveX();
        if(!checkEntityCollisions(0f, xMove))
            moveY();
    }
    
    public void moveX(){
        
        if(xMove > 0){ //kanan
            int tx = (int)(x+ xMove + bounds.x + bounds.width) / Turf.TURFWIDTH;
            if(!collisionWithObj(tx, (int)(y +bounds.y) / Turf.TURFHEIGHT) && 
                    !collisionWithObj(tx , (int)(y + bounds.y + bounds.height) / Turf.TURFHEIGHT)){
                x += xMove;
            }
        }else if(xMove < 0){//kekiri
            int tx = (int)(x+ xMove + bounds.x) / Turf.TURFWIDTH;
            if(!collisionWithObj(tx, (int)(y +bounds.y) / Turf.TURFHEIGHT) && 
                    !collisionWithObj(tx , (int)(y + bounds.y + bounds.height) / Turf.TURFHEIGHT)){
                x += xMove;
            }
        }   
    }
    
    public void moveY(){
        if(yMove < 0){ //ats
            int ty = (int)(y + yMove + bounds.y) / Turf.TURFHEIGHT;
            
            if( !collisionWithObj((int)(x+ bounds.x) / Turf.TURFWIDTH,ty) &&
                (!collisionWithObj((int)(x+ bounds.x) / Turf.TURFWIDTH ,ty))){
                y += yMove;
            }
        }else if(yMove > 0){//bwh
            int ty = (int)(y + yMove + bounds.y + bounds.height) / Turf.TURFHEIGHT;
            
            if( !collisionWithObj((int)(x+ bounds.x) / Turf.TURFWIDTH,ty) &&
                (!collisionWithObj((int)(x+ bounds.x) / Turf.TURFWIDTH ,ty))){
                y += yMove;
            }
        }
    }
    
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    protected boolean collisionWithObj(int x, int y){
        return handler.getWorld().getTurf(x, y).isSolid();
    } 
<<<<<<< HEAD:src/tubesgame/entities/movable/Movable.java
=======
            

 //   public boolean intersects(int x, int y) {
   //     rreturn handler.getWorld
    //}
>>>>>>> 4763d0f857a05a127c3d7684d831107964e8198f:src/tubesgame/entities/creature/Creature.java

    //getset
    
    //position x;
    public float getxPosition() {
        return x;
    }

    public void setxMove(float xMove) {
        this.xMove = xMove;
    }
    //position y
    public float getyPosition() {
        return y;
    }

    public void setyMove(float yMove) {
        this.yMove = yMove;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Handler;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class Lady extends Movable{
    
    private Animations ladyLeft, ladyDown, ladyUp, ladyRight;

    private float dest = 850;
    
    public Lady(Handler handler, float x, float y){
        super(handler, x, y, Turf.TURFWIDTH, Turf.TURFHEIGHT);
        bounds.x = 16;
        bounds.y = 32;
        bounds.width = 32;
        bounds.height = 32;
        
        
        ladyLeft = new Animations(500, Assets.lady_left);
        ladyDown = new Animations(500, Assets.lady_down);
        ladyUp = new Animations(500, Assets.lady_up);
        ladyRight = new Animations(500, Assets.lady_right);
        
    }

    @Override
    public void tick() {
        
        
        ladyLeft.tick();
        ladyDown.tick();
        ladyUp.tick();
        ladyRight.tick();
        moveAround();
        ladyMove();
        
    }
    
    public void moveAround(){
      
        if(dest == 850){
            yMove = -lady_speed;
            if(getyPosition() <= dest){
                dest = 230;
                yMove = 0;
            }
        }
        
        if(dest == 230){
            xMove = -lady_speed;
            if(getxPosition() <= dest){
                dest = 1770;
                xMove = 0;
            }
        }
        
        if(dest == 1770){
            yMove = lady_speed;
            if(getyPosition() >= dest){
                dest = 1050;
                yMove =0;
            }
        }
        
        if(dest == 1050){
            xMove = lady_speed;
            if(getxPosition() >= 1050){
                dest = 850;
                xMove =0;
            }
        }
   


        
   
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),  (int)(x - handler.getGameCamera().getxOffset()), 
                                    (int)(y - handler.getGameCamera().getyOffset()), width, height, null);
    }
    
    
    private BufferedImage getCurrentAnimationFrame(){
        
        if(yMove < 0){
            return ladyUp.getCurrentFrame();
        }else if(xMove < 0){
            return ladyLeft.getCurrentFrame();
        }else if(yMove > 0){
            return ladyDown.getCurrentFrame();
        }else{
            return ladyRight.getCurrentFrame();
        } 
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Handler;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class Wizard extends Movable{

    
    private Animations wizard1;
    
   
    
    public Wizard(Handler handler, float x, float y){
        super(handler, x, y, Turf.TURFWIDTH, Turf.TURFHEIGHT);
        
        bounds.x = 16;
        bounds.y = 32;
        bounds.width = 32;
        bounds.height = 32;
        
        wizard1 = new Animations(250, Assets.wizard1);
        
    }

    @Override
    public void tick() {
        
        
        wizard1.tick();
       
    }
    
    

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),  (int)(x - handler.getGameCamera().getxOffset()), 
                                    (int)(y - handler.getGameCamera().getyOffset()), width, height, null);
        
    }
    
    
    
    private BufferedImage getCurrentAnimationFrame(){
        
        return wizard1.getCurrentFrame();
    }

}

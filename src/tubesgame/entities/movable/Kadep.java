/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Handler;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import tubesgame.gfx.Animations;
import tubesgame.gfx.Assets;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class Kadep extends Movable {
    
    private Animations kadepDown, kadepUp;

    private float dest = 1000;
    
    public Kadep(Handler handler, float x, float y){
        super(handler, x, y, Turf.TURFWIDTH, Turf.TURFHEIGHT);
        bounds.x = 16;
        bounds.y = 32;
        bounds.width = 32;
        bounds.height = 32;
        
        
        kadepDown = new Animations(300, Assets.kadep_down);
        kadepUp = new Animations(300, Assets.kadep_up);
    }

    @Override
    public void tick() {
        
        kadepDown.tick();
        kadepUp.tick();
        moveAround();
        kadepMove();
        
    }
    
    public void moveAround(){
      
        
        if(dest == 1000){
            yMove = -speed/3;
            if(getyPosition() <= dest){
                dest = 1300;
            }
        }
        
        if(dest == 1300){
            yMove = speed/3;
            if(getyPosition() >= dest){
                dest = 1000;
            }
        }
     
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),  (int)(x - handler.getGameCamera().getxOffset()), 
                                    (int)(y - handler.getGameCamera().getyOffset()), width, height, null);
    }
    
    private BufferedImage getCurrentAnimationFrame(){
        if(yMove > 0){
            return kadepDown.getCurrentFrame();
        }else{
            return kadepUp.getCurrentFrame();
        }
    }
}

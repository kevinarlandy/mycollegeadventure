/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.entities.movable;

import collegeadventure.Handler;
import java.awt.Graphics;
import tubesgame.utils.Utils;

/**
 *
 * @author Kevin Arlandy
 */
public class Enemy extends Movable{
    
    
    private Player player;
    
    public Enemy(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
       
    }

    @Override
    public void tick() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void render(Graphics g) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void Update(){
        if(player == null){
            Look();
        }else{
            Chase();
        
            if(Utils.LineOfSight(this, player))
                Attack();
        }
        
        if(player.getHealth() == 0){
            Die();
        }
    }
    protected void Attack(){
        if(player == null);
    }
    
    protected void Look(){
        
    }
    
    protected void Chase(){
        
    }
    
    protected void Die(){
        
    }
    
}

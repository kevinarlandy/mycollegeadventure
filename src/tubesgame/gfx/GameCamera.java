/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.gfx;

import collegeadventure.Game;
import collegeadventure.Handler;
import tubesgame.entities.Entity;
import tubesgame.turf.Turf;

/**
 *
 * @author Kevin Arlandy
 */
public class GameCamera {
    
    private Handler handler;
    private float xOffset, yOffset;
    //
    private float xFog, yFog;
    
    public GameCamera(Handler handler, float xOffset, float yOffset){
        this.handler = handler;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        
        this.xFog = xFog;
        this.yFog = yFog;
    }
    
    //check blank
    public void checkBlankMap(){
        if(xOffset <0){
            xOffset = 0;
        }else if(xOffset > handler.getWorld().getWidth()*Turf.TURFWIDTH - handler.getWidth()){
            xOffset =handler.getWorld().getWidth()*Turf.TURFWIDTH - handler.getWidth();
        }
        if(yOffset <0){
            yOffset =0;
        }else if(yOffset > handler.getWorld().getHeight() * Turf.TURFHEIGHT - handler.getHeight()){
            yOffset =handler.getWorld().getHeight() * Turf.TURFHEIGHT - handler.getHeight();
    
        }
    }
    
    public void centerOnEntity(Entity e){
        xOffset = e.getX() - handler.getWidth() / 2 + e.getWidth() /2;
        yOffset = e.getY() - handler.getHeight() / 2 + e.getHeight() /2;
        
        xFog = e.getX() - handler.getWidth() /3 + e.getWidth() / 3;
        yFog = e.getY() - handler.getHeight() /3 + e.getHeight() / 3;
        checkBlankMap();
    }
    
    public void move(float xAmt, float yAmt){
        xOffset += xAmt;
        yOffset += yAmt;
        
        xFog += yAmt;
        yFog += yAmt;
        checkBlankMap();
    }
    
    
    public float getxOffset() {
        return xOffset;
    }

    public void setxOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public float getyOffset() {
        return yOffset;
    }

    public void setyOffset(float yOffset) {
        this.yOffset = yOffset;
    }
}

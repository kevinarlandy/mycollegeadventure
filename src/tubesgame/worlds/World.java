/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.worlds;

import java.awt.Graphics;
import collegeadventure.Game;
import collegeadventure.Handler;
import tubesgame.entities.EntityManager;
<<<<<<< HEAD
import tubesgame.entities.movable.Kadep;
import tubesgame.entities.movable.Kursi;
import tubesgame.entities.movable.Lady;
import tubesgame.entities.movable.Lecture;
import tubesgame.entities.movable.Player;
import tubesgame.entities.movable.Tuyul;
import tubesgame.entities.movable.Wizard;
import tubesgame.entities.unmovable.Board;
import tubesgame.entities.unmovable.Chair;
import tubesgame.entities.unmovable.Chair2;
import tubesgame.entities.unmovable.Lemari;
import tubesgame.entities.unmovable.Piano;
import tubesgame.entities.unmovable.Plant1;
import tubesgame.entities.unmovable.Table1;
import tubesgame.entities.unmovable.Table2;
import tubesgame.entities.unmovable.Table3;
import tubesgame.entities.unmovable.Table4;
import tubesgame.entities.unmovable.Table5;
import tubesgame.entities.unmovable.Table6;
import tubesgame.entities.unmovable.Table7;
import tubesgame.entities.unmovable.Boneka1;
import tubesgame.entities.unmovable.Boneka2;
import tubesgame.entities.unmovable.Student;
import tubesgame.entities.unmovable.Table8;

=======
import tubesgame.entities.movable.Player;
import tubesgame.entities.movable.Tuyul;
>>>>>>> 4763d0f857a05a127c3d7684d831107964e8198f
import tubesgame.turf.Turf;
import tubesgame.utils.Utils;

/**
 *
 * @author Kevin Arlandy
 */
public class World {
    
    private Handler handler;
    private int width, height;
    private int spawnX, spawnY;

    
    private int[][] turfs;
    //entity
    private EntityManager entityManager;
<<<<<<< HEAD
    
  
    
    public World(Handler handler, String path){
        this.handler = handler;
        
      
        
        
        entityManager = new EntityManager(handler, new Player(handler, 1280,3120));
        //movable ~

        
        entityManager.addEntity(new Tuyul(handler, 500, 2600));
        entityManager.addEntity(new Lady(handler, 1050, 1250));
        entityManager.addEntity(new Kadep(handler, 2300, 1200));
        entityManager.addEntity(new Lecture(handler, 1888,1400));
        entityManager.addEntity(new Kursi(handler, 850, 1438));
        entityManager.addEntity(new Wizard(handler, 200, 200));
        entityManager.addEntity(new Wizard(handler, 300, 200));
        entityManager.addEntity(new Wizard(handler, 400, 200));
        entityManager.addEntity(new Wizard(handler, 500, 200));
        entityManager.addEntity(new Wizard(handler, 600, 200));
        entityManager.addEntity(new Wizard(handler, 700, 200));
        entityManager.addEntity(new Wizard(handler, 800, 200));
        entityManager.addEntity(new Wizard(handler, 900, 200));
        entityManager.addEntity(new Wizard(handler, 1000, 200));
        entityManager.addEntity(new Wizard(handler, 1100, 200));
        entityManager.addEntity(new Wizard(handler, 1200, 200));
        entityManager.addEntity(new Wizard(handler, 1300, 200));
        entityManager.addEntity(new Wizard(handler, 1400, 200));
        entityManager.addEntity(new Wizard(handler, 1500, 200));
        entityManager.addEntity(new Wizard(handler, 1600, 200));
        entityManager.addEntity(new Wizard(handler, 1700, 200));
        entityManager.addEntity(new Wizard(handler, 1800, 200));
        entityManager.addEntity(new Wizard(handler, 1900, 200));
        
        
        entityManager.addEntity(new Board(handler, 600, 990));
        entityManager.addEntity(new Piano(handler, 2336 , 768));
        entityManager.addEntity(new Plant1(handler, 450, 1030));
        entityManager.addEntity(new Lemari(handler, 815, 1020));
        entityManager.addEntity(new Boneka1(handler, 450, 1525));
        entityManager.addEntity(new Boneka2(handler, 1788, 1484));
        entityManager.addEntity(new Student(handler, 1280,1280));
        
        
        //table+chair classroom
        entityManager.addEntity(new Table1(handler, 768, 1408));
        entityManager.addEntity(new Chair(handler, 776, 1438));
        
        entityManager.addEntity(new Table1(handler, 768, 1280));
        entityManager.addEntity(new Chair(handler, 776, 1310));
        
        entityManager.addEntity(new Table2(handler, 640, 1280));
        entityManager.addEntity(new Chair(handler, 648, 1310));
        
        entityManager.addEntity(new Table3(handler, 512, 1280));
        entityManager.addEntity(new Chair(handler, 520, 1310));
        
        entityManager.addEntity(new Table4(handler, 640, 1408));
        entityManager.addEntity(new Chair(handler, 648, 1438));
        
        entityManager.addEntity(new Table5(handler, 512, 1408));
        entityManager.addEntity(new Chair(handler, 520, 1438));
 
        //departemen

        entityManager.addEntity(new Table6(handler, 1856,832));
        entityManager.addEntity(new Chair2(handler, 1880,790));
        
        entityManager.addEntity(new Table7(handler, 2064, 832));
        entityManager.addEntity(new Chair2(handler, 2085, 790));
        
        
        
=======
    
    public World(Handler handler, String path){
        this.handler = handler;
        //add object
        entityManager = new EntityManager(handler, new Player(handler, 640, 1500));
        entityManager.addEntity(new Tuyul(handler, 200, 200));
>>>>>>> 4763d0f857a05a127c3d7684d831107964e8198f
        
        
        loadWorld(path);
        
        entityManager.getPlayer().setX(spawnX);
        entityManager.getPlayer().setY(spawnY);
    }
    
    public void tick(){
        entityManager.tick();
    }
    
    public void render(Graphics g){
        int xStart = (int) Math.max(0, handler.getGameCamera().getxOffset() / Turf.TURFWIDTH);
        int xEnd = (int) Math.min(width, (handler.getGameCamera().getxOffset() + handler.getWidth()) / Turf.TURFWIDTH + 1);
        int yStart = (int) Math.max(0, handler.getGameCamera().getyOffset() / Turf.TURFHEIGHT);
        int yEnd = (int) Math.min(height, (handler.getGameCamera().getyOffset() + handler.getHeight()) / Turf.TURFHEIGHT + 1);
        
        for(int y = yStart; y < yEnd; y++){
            for(int x = xStart; x < xEnd; x++){
                getTurf(x, y).render(g, (int)(x * Turf.TURFWIDTH - handler.getGameCamera().getxOffset()), 
                                        (int)(y * Turf.TURFHEIGHT - handler.getGameCamera().getyOffset()));
            }
        }
        //entity
        entityManager.render(g);
    }
    
    
    //
    public Turf getTurf(int x, int y){
        
        if (x<0 || y< 0 || x >= width || y>= height){
            return Turf.wooden1floor;
        }
        Turf t = Turf.turfs[turfs[x][y]];
        if(t == null){
<<<<<<< HEAD
            return Turf.wooden1floor;
=======
            return Turf.blank;
>>>>>>> 4763d0f857a05a127c3d7684d831107964e8198f
        }
        return t;
    }
    
    private void loadWorld(String path){
        String file = Utils.loadFileAsString(path);
        String[] tokens = file.split("\\s+");
        width = Utils.parseInt(tokens[0]);
        height = Utils.parseInt(tokens[1]);
        spawnX = Utils.parseInt(tokens[2]);
        spawnY = Utils.parseInt(tokens[3]);
        
        turfs = new int[width][height];
        for(int y = 0; y< height; y++){
            for(int x = 0; x < width; x++){
                turfs[x][y] = Utils.parseInt(tokens[(x + y * width) + 4]);
            }
        }
    }
    
<<<<<<< HEAD
=======
    
>>>>>>> 4763d0f857a05a127c3d7684d831107964e8198f
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    //ngilangin blankspace
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }
}

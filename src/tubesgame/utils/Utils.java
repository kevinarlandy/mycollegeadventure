/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import tubesgame.entities.movable.Movable;

/**
 *
 * @author Kevin Arlandy
 */
public class Utils {
    
    //
    public static boolean LineOfSight(Movable go1, Movable go2){
        return true;
    }
    
    public static float dist(float x1 , float y1, float x2, float y2){
        double x = x2 -x1 ;
        double y = y2 - y1;
        
        return (float)Math.sqrt((x*x)+ (y*y));
    }
    
    public static String loadFileAsString(String path){
        StringBuilder builder = new StringBuilder();
        
        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            while((line = br.readLine()) != null)
                builder.append(line + "\n");
            
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        
        return builder.toString();
    }
    
    public static int parseInt(String number){
        try{
            return Integer.parseInt(number);
        }catch(NumberFormatException e){
            e.printStackTrace();
            return 0;
        }
    }
}

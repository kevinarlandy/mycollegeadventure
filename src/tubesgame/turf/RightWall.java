/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.turf;

import tubesgame.gfx.Assets;

/**
 *
 * @author Kevin Arlandy
 */
public class RightWall extends Turf{
    
    public RightWall(int id){
        super(Assets.rightwall, id);
    }
    
    public boolean isSolid(){
        return true;
    }
}

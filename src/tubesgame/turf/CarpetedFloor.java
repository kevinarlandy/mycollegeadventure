/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.turf;

import tubesgame.gfx.Assets;

/**
 *
 * @author Kevin Arlandy
 */
public class CarpetedFloor extends Turf{
    
    public CarpetedFloor(int id){
        super(Assets.carpet1, id);
    }
}

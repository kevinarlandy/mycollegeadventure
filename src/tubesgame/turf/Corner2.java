/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesgame.turf;

import tubesgame.gfx.Assets;

/**
 *
 * @author Kevin Arlandy
 */
public class Corner2 extends Turf {
    
    public Corner2(int id){
        super(Assets.corner2, id);
    }
    
    public boolean isSolid(){
        return true;
    }
}

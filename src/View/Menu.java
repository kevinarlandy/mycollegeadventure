/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.BevelBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.table.*;
import javax.swing.border.TitledBorder;
import InGame.*;
import DB.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.util.Locale;

/**
 *
 * @author Davin Yulion, Willy Octavian, Hanna Veronica, Kevin Arlandy
 */
public class Menu extends JFrame implements KeyListener {

    Menu() {
        this.setLayout(null);
        this.setUndecorated(true);
//        playSound();
        awal();
    }

    public static void main(String[] args) {
        new Menu().setVisible(true);

    }

    private void awal() {
        setExtendedState(JFrame.MAXIMIZED_BOTH);

//        setLocationRelativeTo(null);
        setTitle("College Adventure");
        String menuUtama = "img/background_awal.jpg";
        String menuNewGame = "img/background_daftar3.jpg";
        Image background = resizeImage(menuUtama, 1400, 770);
        Image background_daftar = resizeImage(menuNewGame, 760, 400);
        setContentPane(new JLabel(new ImageIcon(background)));
        addKeyListener(this);

       
        tt.setLayout(null);

        this.add(tt);
        
        t1.start();
        // pengaturan panel awal
        repaint();
    }

    //GUI awal" new Game,LoadGame,Option
    private void menu_Awal() {
        panelAwal.setVisible(true);
        panelAwal.setSize(500, 600);
        panelAwal.setLocation(0, 250);
        panelAwal.setOpaque(false);
        this.add(panelAwal);

//        lblLogo.setIcon(new ImageIcon("img/logo.png"));
//        lblLogo.setSize(300, 100);
//        lblLogo.setLocation(120, 50);
//        panelAwal.add(lblLogo);
        btnExit.setSize(50, 50);
        btnExit.setLocation(1310, 0);
        btnExit.setIcon(new ImageIcon(resizeImage("img/exit.png", 85, 50)));
        add(btnExit);

        btnNewGame.setIcon(new ImageIcon("img/newgame.png"));
        btnNewGame.setSize(250, 50);
        btnNewGame.setLocation(130, 200);

        panelAwal.add(btnNewGame);

        btnLoadGame.setIcon(new ImageIcon("img/loadgame.png"));
        btnLoadGame.setSize(250, 50);
        btnLoadGame.setLocation(130, 270);
        panelAwal.add(btnLoadGame);
        repaint();

        //Untuk EXIT 
        btnExit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
                connect.logOff();
            }
        });

        //Ke panel New Game
        btnNewGame.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new NewGame().setVisible(true);
                setVisible(false);
            }
        });

        //untuk gender button
        //Ke Panel LoadGame
        btnLoadGame.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new ScoreBoard().setVisible(true);
                setVisible(false);
            }
        });

    }

    //Untuk Me Rize Image
    private Image resizeImage(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }

    //Untuk memasukan Musik
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("music/openingmusik.wav").getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            t1.stop();
            tt.setVisible(false);
            menu_Awal();
        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            lblNewGame.setVisible(false);
            lblscore.setVisible(false);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
    static Clip clip;

    Koneksi connect;

    Dimension d = new Dimension();
    JPanel panelAwal = new JPanel();

    JPanel paneldaftar = new JPanel();
    JPanel panelscore = new JPanel();
    JPanel panelgender = new JPanel();

    JLabel lblNewGame = new JLabel();
    JLabel lblscore = new JLabel();
    JLabel lblLogo = new JLabel();
    JLabel lblNama = new JLabel();
    JLabel lbldaftarnama = new JLabel();
    JLabel lblgendericon = new JLabel();
    JLabel lblskill = new JLabel();

    JTextField daftarnama_txt = new JTextField(15);

    JTextArea TAskillinfo = new JTextArea();

    JButton btnLoadGame = new JButton();
    JButton btnNewGame = new JButton();
    JButton btnOption = new JButton();
    JButton btnExit = new JButton();
    JButton buttongo = new JButton();
    JButton btnhome;
    JButton btnhome2 = new JButton();

    JRadioButton btnFemale = new JRadioButton("Female");
    JRadioButton btnMale = new JRadioButton("Male");
    ButtonGroup btngroup = new ButtonGroup();

    JScrollPane skillinfo1 = new JScrollPane();
    JScrollPane jps;
    Player P;

    String[] kolomtabel = {"Nama", "Point", "Time Play"};
    // object
    private Color color; //untuk background color 

    DefaultTableModel Tmodel;
    JTable tabellistplayer = new JTable();
    
     BlinkedText tt = new BlinkedText();
     Thread t1 = new Thread(tt);
}

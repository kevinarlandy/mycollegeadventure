/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DB.Koneksi;
import DB.QueryDB;
import InGame.Player;
import collegeadventure.*;
import static collegeadventure.Game.playSound;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 *
 * @author willy
 */
public class NewGame extends JFrame {

    public NewGame() {
        initComponents();
        connect = new Koneksi();
        connect.logOn();
        this.setUndecorated(true);
        this.setLayout(null);
    }

    private void initComponents() {
        this.setBounds(300, 50, 800, 600);

        lblNewGame.setSize(800, 600);
        lblNewGame.setLayout(null);
        lblNewGame.setIcon(new ImageIcon(resizeImage("img/NewGame/background_daftar.jpg", 800, 600)));

        //panel untuk daftar character
        paneldaftar.setLayout(null);
        paneldaftar.setSize(760, 400);
        paneldaftar.setLocation(20, 170);
        paneldaftar.setBackground(new Color(0, 0, 0, 180));

        //label nama
        lbldaftarnama.setBounds(280, 20, 300, 50);
        lbldaftarnama.setText("ENTER YOUR NAME ! ");
        lbldaftarnama.setFont(new Font("Serif", Font.BOLD, 20));
        lbldaftarnama.setForeground(Color.red);

        //text isi nama
        daftarnama_txt.setBounds(290, 80, 180, 25);
        daftarnama_txt.setBackground(Color.black);
        daftarnama_txt.setForeground(Color.white);

        BevelBorder raisedBevel = (BevelBorder) BorderFactory.createBevelBorder(BevelBorder.RAISED);
        BevelBorder loweredBevel = (BevelBorder) BorderFactory.createBevelBorder(BevelBorder.LOWERED);
        Border border = BorderFactory.createCompoundBorder(raisedBevel, loweredBevel);
        paneldaftar.setBorder(border);

        //untuk pilih gender
        btnFemale.setBounds(270, 120, 100, 30);
        btnFemale.setOpaque(false);
        btnFemale.setForeground(Color.white);
        btnMale.setBounds(430, 120, 100, 30);
        btnMale.setOpaque(false);
        btnMale.setForeground(Color.white);
        btngroup.add(btnMale);
        btngroup.add(btnFemale);

        //panel gender info
        panelgender.setLayout(null);
        panelgender.setBounds(200, 170, 400, 180);
        panelgender.setBackground(Color.WHITE);
        lblgendericon.setBounds(20, 15, 150, 150);
        lblgendericon.setIcon(new ImageIcon(resizeImage("img/NewGame/gender_icon.jpg", 150, 150)));
        TAskillinfo.setBounds(180, 15, 200, 150);
        TAskillinfo.setLineWrap(true); //biar kata/kalimat meyesuaikan textarea 
        TAskillinfo.setWrapStyleWord(true); //supaya wordnya itu rata kiri
        TAskillinfo.setEditable(false); // biar gabisa di edit
        TAskillinfo.setBackground(Color.WHITE);
        TAskillinfo.setForeground(Color.RED); // mewarnai tulisan dalam textarea 
        TAskillinfo.setBorder(new TitledBorder("Cerita : ")); // memberi border pada textarea diatas
        TAskillinfo.setText("-------------------------");

        //button untuk go and back
        buttongo.setBounds(680, 320, 70, 70);
        buttongo.setIcon(new ImageIcon(resizeImage("img/NewGame/button_go.jpg", 70, 70)));
        btnhome.setBounds(20, 60, 90, 90);
        btnhome.setIcon(new ImageIcon(resizeImage("img/NewGame/home.jpg", 90, 90)));
//        lblNama.setSize(100, 30);
//        lblNama.setLocation(350, 30);

        //atribut panel daftar
        paneldaftar.add(lblNama);
        paneldaftar.add(lbldaftarnama);
        paneldaftar.add(daftarnama_txt);
        paneldaftar.add(lbldaftarnama);
        paneldaftar.add(btnFemale);
        paneldaftar.add(btnMale);
        paneldaftar.add(buttongo);
        paneldaftar.add(panelgender);
        //atribut panelgender
        panelgender.add(lblgendericon);
        panelgender.add(TAskillinfo);
        //atribut panel new game
        lblNewGame.add(paneldaftar);
        lblNewGame.add(btnhome);

        lblNama.setBounds(250, 20, 395, 127);
        lblNama.setIcon(new ImageIcon(resizeImage("img/NewGame/daftarfix1.png", 300, 127)));
        lblNewGame.add(lblNama);

        this.add(lblNewGame);
        repaint();

        btnhome.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                setVisible(false);
                new Menu().setVisible(true);
            }
        });

        btnFemale.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                genderFemale();
            }
        });

        btnMale.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                genderMale();
            }
        });

        buttongo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String a = daftarnama_txt.getText();
                if (daftarnama_txt.getText() == "") {
                    JOptionPane.showMessageDialog(null, "Tolong isi nama :");
                    System.out.println("NO NAME !");
                } else {
                    Player p = new Player();
                    p.setNama(daftarnama_txt.getText());
                    QueryDB.insertData(p);
                    Game game = new Game("Stage01", 800, 600);
                    game.start();
                    playSound();

                }
            }
        });

    }

    private void genderFemale() {
        lblgendericon.setIcon(new ImageIcon(resizeImage("img/NewGame/gender_female.jpg", 150, 150)));
        TAskillinfo.setText("\nPerempuan ini bernama Elly yang tinggal di hutan Mory. Suatu saat dia tersesat saat mencari "
                + "bahan untuk makan malam, lalu disitulah cerita dimulai.....");
    }

    private void genderMale() {
        lblgendericon.setIcon(new ImageIcon(resizeImage("img/NewGame/gender_male.jpg", 150, 150)));
        TAskillinfo.setText("\nLaki-laki ini bernama Zen, penebang hutan yang gagah. Saat membawa pulang kayu yang telah "
                + "dia tebang, langit mendadak gelap dan terdengar suara aneh memanggil namanya .... ");
    }

    private Image resizeImage(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }

//    public static void main(String[] args) {
//        new NewGame().setVisible(true);
//    }

    Koneksi connect;

    JPanel panelAwal = new JPanel();

    JPanel paneldaftar = new JPanel();
    JPanel panelLoad = new JPanel();
    JPanel panelgender = new JPanel();

    JLabel lblNewGame = new JLabel();
    JLabel lblNama = new JLabel();
    JLabel lbldaftarnama = new JLabel();
    JLabel lblgendericon = new JLabel();
    JLabel lblskill = new JLabel();

    JTextField daftarnama_txt = new JTextField(15);

    JTextArea TAskillinfo = new JTextArea();

    JButton btnLoadGame = new JButton();
    JButton btnNewGame = new JButton();
    JButton btnOption = new JButton();
    JButton buttongo = new JButton();
    JButton btnhome = new JButton();

    JRadioButton btnFemale = new JRadioButton("Female");
    JRadioButton btnMale = new JRadioButton("Male");
    ButtonGroup btngroup = new ButtonGroup();

    JScrollPane skillinfo1 = new JScrollPane();

}

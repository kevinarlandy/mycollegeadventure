/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DB.*;
import InGame.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author willy
 */
public class ScoreBoard extends JFrame {

    private JPanel pnlscore;
    private static JTable tbllist;
    JButton btnhome = new JButton();

    public ScoreBoard() {
        initComponents();
        showlist();
        this.setLayout(null);
        this.setUndecorated(true);
    }

    public void initComponents() {
        this.setBounds(300, 50, 800, 600);

        String menuUtama = "img/LoadGame/background_daftar.jpg";
        Image background = resizeImage(menuUtama, 800, 600);
        setContentPane(new JLabel(new ImageIcon(background)));

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("LIST PLAYER");
        setLayout(null);

        btnhome.setBounds(20, 60, 90, 90);
        btnhome.setIcon(new ImageIcon(resizeImage("img/LoadGame/home.jpg",90,90)));
        
        
        //untuk panel score menampilkan tabel list yang sudah buat akun 
        pnlscore = new JPanel();
        pnlscore.setLayout(null);
        pnlscore.setBackground(new Color(0, 0, 0, 180));
        pnlscore.setBounds(20, 170, 760, 400);
        

        String[] columnNames = {"Nama", "Point", "Time Play"};

        tbllist = new JTable();
        DefaultTableModel models = (DefaultTableModel) tbllist.getModel();
        for (int i = 0; i < columnNames.length; i++) {
            models.addColumn(columnNames[i]);
        }
        //    models.

        tbllist.setBackground(Color.BLACK);
        tbllist.setForeground(Color.WHITE);
        tbllist.setOpaque(false);
        tbllist.setColumnSelectionAllowed(false);

        JScrollPane jps = new JScrollPane(tbllist);
        jps.setBounds(100, 50, 560, 300);
        
        //untuk atribut panelscore
        pnlscore.add(jps);
        //untuk atribut frame
        this.add(btnhome);
        this.add(pnlscore);
        
        btnhome.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent me) {
                new Menu().setVisible(true);
                setVisible(false);
            }
            
        });
    }

    private Image resizeImage(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }

    public static void showlist() {

        ArrayList<Player> listp = (ArrayList<Player>) QueryDB.LoadGame();
        DefaultTableModel model = (DefaultTableModel) tbllist.getModel();
        Object[] rows = new Object[4];

        for (int i = 0; i < listp.size(); i++) {
            rows[0] = listp.get(i).getNama();
            rows[1] = listp.get(i).getPoint();
            rows[2] = listp.get(i).getTimeplay();

            model.addRow(rows);

        }
    }

//    public static void main(String[] args) {
//        new ScoreBoard().setVisible(true);
//    }
    
}
